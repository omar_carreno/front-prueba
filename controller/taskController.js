/*global angular:true*/
const taskController = function (todoSrv, $scope, tokenSrv) {
  const vm = this;
  vm.fileToUpload = null;
  vm.ext = null;
  $scope.pass = true;
  $scope.uploadFile = () => {
    tokenSrv.getToken()
      .then((token) => {
        return todoSrv.createTodo(vm.fileToUpload, token)
      })
      .then(() => {
        vm.res = "Data actualizada";
      })
      .catch((err) => {
        vm.res = "Data no actualizada: " + err;
      });
  }
  $scope.login = () => {
    if (vm.email && vm.password && $scope.pass) {
      $scope.pass = false;
      todoSrv.authUserWithMail(vm.email, vm.password)
        .then(() => {
          $scope.auth = true;
          $scope.pass = true;
        })
        .catch((err) => {
          vm.error = err;
          $scope.auth = false;
          $scope.pass = true;
        });
    }

  };
  $scope.file_changed = function (element) {
    var fileReader = new FileReader();
    fileReader.readAsDataURL(element.files[0]);
    fileReader.onload = function (event) {
      vm.ext = $scope.getExtensionFile(element.files[0].name);
      vm.fileToUpload = event.target.result.split(",")[1];
    };
  };
  $scope.getExtensionFile = (string) => {
    const length = string.length;
    const dot = string.indexOf(".");
    return string.slice(dot, length);
  };
};
taskController.$inject = ["todoSrv", "$scope", "tokenSrv"];
angular.module("phonecatApp").controller("taskController", taskController);