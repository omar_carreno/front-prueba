# Prueba Back Front

## Configurar Ambiente

1. Instalar http-server `npm install http-server -g`

## Desplegar localmente

1. Ubicarse en la capeta raiz.
2. Ejecutar el comando `http-server ./ -p :port`. *:port* es el puerto que se vaya a utilizar.
