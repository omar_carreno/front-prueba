/*global angular:true*/
/*global firebase:true*/

const todoSrv = ($q, $firebaseAuth) => {
  const responseTodoSrv = {
    createTodo: (file, token) => {
      return $q((resolve, reject) => {
        const config = {
          headers: {
            "Authorization": `${token}`,
          },
        };
        axios.post('http://localhost:9090/importExcel/imporExcel', { file }, config)
          .then(function (response) {
            resolve(response.data);
          })
          .catch(function (error) {
            reject(error.data);
          });
      });
    },
    authUserWithMail: function (email, password) {
      return $q((resolve, reject) => {
        $firebaseAuth().$signInWithEmailAndPassword(email, password)
          .then((authData) => {
            resolve(authData);
          }).catch((error) => {
            reject(error);
          });
      });
    },
  };
  return responseTodoSrv;
};
todoSrv.$inject = ["$q", "$firebaseAuth"];
angular.module("phonecatApp").factory("todoSrv", todoSrv);