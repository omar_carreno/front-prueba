/*global angular: true*/
/*global firebase: true*/

var tokenSrv = ($q) => {
    var response = {
        getToken: () => $q((resolve, reject) => {
            if (firebase.auth().currentUser) {
                firebase.auth().currentUser.getIdToken(false)
                .then((token) => {
                    resolve(token);
                })
                .catch((error) => {
                    reject(error);
                });
            } else {
                reject();
            }
        })
    };
    return response;
};
tokenSrv.$inject = ["$q"];
angular.module("phonecatApp").factory("tokenSrv", tokenSrv);