/*global angular:true*/

const runHome = function () {
	"use strict";
};
runHome.$inject = [];
angular
	.module("phonecatApp", ["firebase"])
	.run(runHome);

firebase.initializeApp(firebaseConfig);